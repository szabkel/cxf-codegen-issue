package com.test;

import hu.mnb.webservices.MNBArfolyamServiceSoapGetCurrenciesStringFaultFaultMessage;
import hu.mnb.webservices.MNBArfolyamServiceSoapImpl;
import hu.mnb.webservices.ObjectFactory;

public class SoapTestCaseRunner {
    public void Run() throws MNBArfolyamServiceSoapGetCurrenciesStringFaultFaultMessage {
        var of = new ObjectFactory();
        var c = new MNBArfolyamServiceSoapImpl();

        var service = c.getCustomBindingMNBArfolyamServiceSoap();
        var res = service.getCurrencies(of.createGetCurrenciesRequestBody());
        System.out.println(res);
    }
}
